package co.com.pragma.service.impl;

import java.util.stream.Collectors;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

import co.com.pragma.amazonaws.lambda.Request;
import co.com.pragma.entity.Customer;

public class CustomerService {
	public static Object listByAge(Request request) {
		AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
		DynamoDBMapper mapper = new DynamoDBMapper(db);
		return mapper.scan(Customer.class, new DynamoDBScanExpression()).stream()
				.filter(c -> c.getAge() >= request.getAge()).collect(Collectors.toList());

	}
}
