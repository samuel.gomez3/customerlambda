package co.com.pragma.amazonaws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import co.com.pragma.service.impl.CustomerService;

public class Handler implements RequestHandler<Request, Object> {

	@Override
	public Object handleRequest(Request request, Context context) {
		return CustomerService.listByAge(request);

	}

}
