package co.com.pragma.service.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import co.com.pragma.amazonaws.lambda.Request;
import co.com.pragma.entity.Customer;

public class CustomerService {
	public static Object update(Request request) {
		AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
		DynamoDBMapper mapper = new DynamoDBMapper(db);
		Customer customer = mapper.load(Customer.class, request.getId());
		customer.setAge(request.getCustomer().getAge());
		customer.setCityOfBirth(request.getCustomer().getCityOfBirth());
		customer.setDocumentNumber(request.getCustomer().getDocumentNumber());
		customer.setNames(request.getCustomer().getNames());
		customer.setSurnames(request.getCustomer().getSurnames());
		customer.setTypeOfDocument(request.getCustomer().getTypeOfDocument());
		mapper.save(customer);
		return customer;
	}

}
