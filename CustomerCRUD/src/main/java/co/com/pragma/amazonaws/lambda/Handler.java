package co.com.pragma.amazonaws.lambda;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<Request, Object> {

	@Override
	public Object handleRequest(Request request, Context context) {
		AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
		DynamoDBMapper mapper = new DynamoDBMapper(db);
		Customer customer = null;
		switch (request.getHttpMethod()) {
		case "GET":
			if (request.getId() == 0) {
				return mapper.scan(Customer.class, new DynamoDBScanExpression());
			} else {
				return mapper.load(Customer.class, request.getId());
			}
		case "POST":
			customer = request.getCustomer();
			mapper.save(customer);
			return customer;
		case "DELETE":
			customer = mapper.load(Customer.class, request.getId());
			mapper.delete(customer);
			return customer;
		}
		return "Http Method invalid";
	}

}
