package co.com.pragma.amazonaws.lambda;

import lombok.Data;

@Data
public class Request {

	private int id;
	private String httpMethod;
	private Customer customer;

}
