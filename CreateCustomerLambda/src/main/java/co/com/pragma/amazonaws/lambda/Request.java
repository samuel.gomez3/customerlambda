package co.com.pragma.amazonaws.lambda;

import co.com.pragma.entity.Customer;
import lombok.Data;

@Data
public class Request {
	private Customer customer;
}
