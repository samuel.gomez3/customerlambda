package co.com.pragma.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "customer")
public class Customer {
	@DynamoDBHashKey
	private int id;
	@DynamoDBAttribute
	private String names;
	@DynamoDBAttribute
	private String surnames;
	@DynamoDBAttribute
	private String typeOfDocument;
	@DynamoDBAttribute
	private String documentNumber;
	@DynamoDBAttribute
	private int Age;
	@DynamoDBAttribute
	private String cityOfBirth;

}
