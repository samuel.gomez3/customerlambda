package co.com.pragma.model;

import lombok.Data;

@Data
public class Document {
	private String typeOfDocument;
	private String documentNumber;
}
