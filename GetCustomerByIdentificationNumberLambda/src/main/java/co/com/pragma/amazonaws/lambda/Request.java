package co.com.pragma.amazonaws.lambda;

import co.com.pragma.model.Document;
import lombok.Data;

@Data
public class Request {

	private Document document;

}
