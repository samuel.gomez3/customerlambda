package co.com.pragma.service.impl;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

import co.com.pragma.amazonaws.lambda.Request;
import co.com.pragma.entity.Customer;

public class CustomerService {
	
	public static Object delete(Request request) {
		AmazonDynamoDB db = AmazonDynamoDBClientBuilder.defaultClient();
		DynamoDBMapper mapper = new DynamoDBMapper(db);
		Customer customer = null;
		customer = mapper.load(Customer.class, request.getId());
		mapper.delete(customer);
		return customer;
	}
}
